# K3D Launch

## Using a vars file

```bash
# (optional) Copy and edit with your values
cp terraform.tfvars.example terraform.tfvars

terraform init
terraform plan
terraform apply
```
