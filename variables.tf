variable "k3s_version" {
  description = "The K3s version to use"
  type        = string
  default     = "latest"
}

variable "k3d_cluster_name" {
  default = "big-bang-dev"
  type    = string
}

variable "k3d_cluster_port" {
  default = 6445
  type    = number
}

variable "k3d_cluster_ip" {
  default = "0.0.0.0"
  type    = string
}

variable "k3d_host_lb_port" {
  default = null
  type    = number
}

variable "k3d_cluster_lb_port" {
  default = 80
  type    = number
}

variable "image_cache_path" {
  type    = string
  default = "~/.k3d-bb-test"
}

variable "k3d_args" {
  type    = string
  default = ""
}

variable "server_count" {
  default = 1
  type    = number
}

variable "agent_count" {
  default = 0
  type    = number
}
