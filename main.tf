provider "random" {}

terraform {
  required_version = ">= 1.5.7"
}

resource "random_integer" "port" {
  min = 8000
  max = 8099
}

locals {
  host_lb_port = (var.k3d_host_lb_port != "" ? var.k3d_host_lb_port : random_integer.port.result)
}

resource "null_resource" "k3d" {
  triggers = {
    cluster_name  = var.k3d_cluster_name
    agent_count   = var.agent_count
    server_count  = var.server_count
    ip            = var.k3d_cluster_ip
    port          = var.k3d_cluster_port
    k3s_version   = var.k3s_version
  }


  provisioner "local-exec" {
    command = <<-EOT
      k3d cluster create ${var.k3d_cluster_name} \
        --agents ${var.agent_count} \
        --servers ${var.server_count} \
        --api-port ${var.k3d_cluster_ip}:${var.k3d_cluster_port} \
        --port ${local.host_lb_port}:${var.k3d_cluster_lb_port}@loadbalancer \
        --image rancher/k3s:${var.k3s_version} \
        --k3s-arg "--disable=traefik@server:0"
    EOT
  }

  provisioner "local-exec" {
    when    = destroy
    command = "k3d cluster delete ${self.triggers.cluster_name}"
  }
}

